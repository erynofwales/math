/* Math.hh
 * Eryn Wells <eryn@erynwells.me>
 */

#pragma once

#include "Matrix.hh"
#include "Matrix4.hh"
#include "Vector4.hh"
