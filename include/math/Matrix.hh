/* Matrix.hh
 * Eryn Wells <eryn@erynwells.me>
 */

#pragma once

#include <array>
#include <cmath>
#include <sstream>
#include <stdexcept>

#include "math/Types.hh"

namespace math {

template<typename T, const int N, const int M>
struct Matrix
{
    typedef Matrix<T,N,M> Type;
    typedef T ElemType;
    typedef std::array<ElemType, N*M> ArrayType;

    static const int sRows = N;
    static const int sCols = M;
    static const int sElems = sRows * sCols;

    /** Create a zero matrix. */
    static Type
    zero()
        noexcept
    {
        return Type(0);
    }

    /** Create an identity matrix. */
    static Type
    identity()
        noexcept
    {
        Type mat(0);
        for (int i = 0; i < sRows || i < sCols; i++) {
            mat(i,i) = 1.0;
        }
        return mat;
    }

    /** Default constructor. Does nothing. */
    inline
    Matrix()
        noexcept
    { }

    /**
     * Build a matrix consisting entirely of a single value.
     *
     * @param [in] v    Value to fill the matrix with.
     */
    explicit inline
    Matrix(ElemType v)
        noexcept
    {
        m.fill(v);
    }

    explicit inline
    Matrix(ArrayType a)
        : m(a)
    { }

    /** Copy constructor. */
    inline
    Matrix(const Type& rhs)
        noexcept
        : m(rhs.m)
    { }

    /** Assignment operator. */
    inline Type&
    operator=(const Type& rhs)
        noexcept
    {
        m = rhs.m;
        return *this;
    }

    /**
     * @defgroup Equality
     * @{
     */

    inline bool
    operator==(const Type& rhs)
        const noexcept
    {
        for (int i = 0; i < sElems; i++) {
            if (m[i] != rhs.m[i]) {
                return false;
            }
        }
        return true;
    }

    inline bool
    operator!=(const Type& rhs)
        const noexcept
    {
        return !operator==(rhs);
    }

    /** @} */

    /**
     * @defgroup Element Access
     * @{
     */

    inline ElemType&
    operator()(UInt col,
               UInt row)
    {
        if (col >= sCols || row >= sRows) {
            std::stringstream ss;
            ss << "Invalid indexes: (" << col << ", " << row << ")";
            throw std::out_of_range(ss.str());
        }
        return m[col*sCols + row];
    }

    inline ElemType
    operator()(UInt col,
               UInt row)
        const
    {
        if (col >= sCols || row >= sRows) {
            std::stringstream ss;
            ss << "Invalid indexes: (" << col << ", " << row << ")";
            throw std::out_of_range(ss.str());
        }
        return m[col*sCols + row];
    }

    inline ElemType&
    operator[](UInt i)
    {
        if (i >= sElems) {
            std::stringstream ss;
            ss << "Index out of range: " << i;
            throw std::out_of_range(ss.str());
        }
        return m[i];
    }

    inline ElemType
    operator[](UInt i)
        const
    {
        if (i >= sElems) {
            std::stringstream ss;
            ss << "Index out of range: " << i;
            throw std::out_of_range(ss.str());
        }
        return m[i];
    }

    /** @} */

    /**
     * @defgroup Matrix addition
     * @{
     */

    inline Type
    operator+(const Type& rhs)
        const noexcept
    {
        return Type(*this) += rhs;
    }

    inline Type&
    operator+=(const Type& rhs)
        noexcept
    {
        for (int i = 0; i < sElems; i++) {
            m[i] += rhs.m[i];
        }
        return *this;
    }

    inline Type
    operator-(const Type& rhs)
        const noexcept
    {
        return Type(*this) -= rhs;
    }

    inline Type&
    operator-=(const Type& rhs)
        noexcept
    {
        for (int i = 0; i < sElems; i++) {
            m[i] -= rhs.m[i];
        }
        return *this;
    }

    /** @} */

    /**
     * @defgroup Scalar multiplication
     * @{
     */

    inline Type
    operator*(ElemType rhs)
        const noexcept
    {
        return Type(*this) *= rhs;
    }

    inline Type&
    operator*=(ElemType rhs)
        noexcept
    {
        for (int i = 0; i < sElems; i++) {
            m[i] *= rhs;
        }
        return *this;
    }

    /** @} */

    /**
     * @defgroup Matrix multiplication
     * @{
     */

    template<int P>
    inline Matrix<T,N,P>
    operator*(const Matrix<T,M,P>& rhs)
        const noexcept
    {
        Matrix<T,N,P> out;
        for (int j = 0; j < N; j++) {
            for (int i = 0; i < P; i++) {
                ElemType sum(0);
                for (int k = 0; k < M; k++) {
                    sum += operator()(k,j) * rhs(i,k);
                }
                out(i,j) = sum;
            }
        }
        return out;
    }

    /** @} */

    /** Negates a copy of the matrix. */
    inline Type
    operator-()
        const noexcept
    {
        return *this * -1;
    }

    /** The number of elements in the matrix. */
    inline size_t
    size()
        const noexcept
    {
        return sElems;
    }

protected:
    /** Column major matrix data. */
    ArrayType m;
};

template<typename T, int N>
using Vector = Matrix<T, N, 1>;

} /* namespace math */
