/* Matrix4.hh
 * Eryn Wells <eryn@erynwells.me>
 */

#pragma once

#include "Matrix.hh"
#include "Vector4.hh"

namespace math {

template<typename T>
struct Matrix4
    : public Matrix<T,4,4>
{
    static Matrix4<T>
    translate(T dx,
              T dy,
              T dz)
        noexcept
    {
        Matrix4<T> mat = Matrix4<T>::identity();
        mat[12] = dx;
        mat[13] = dy;
        mat[14] = dz;
        return mat;
    }

    static Matrix4<T>
    rotate(T rx,
           T ry,
           T rz)
        noexcept
    {
        const T sinRx = std::sin(rx);
        const T cosRx = std::cos(rx);
        const T sinRy = std::sin(ry);
        const T cosRy = std::cos(ry);
        const T sinRz = std::sin(rz);
        const T cosRz = std::cos(rz);

        Matrix4<T> mat = Matrix4<T>::identity();
        mat[0]  = cosRy * cosRz;
        mat[1]  = -(cosRy * sinRz);
        mat[2]  = sinRy;
        mat[4]  = cosRx * sinRz + sinRx * sinRy * cosRz;
        mat[5]  = cosRx * cosRz - sinRx * sinRy * sinRz;
        mat[6]  = -(sinRx * cosRy);
        mat[8]  = sinRx * sinRz - cosRx * sinRy * cosRz;
        mat[9]  = sinRx * cosRz + cosRx * sinRy * sinRz;
        mat[10] = cosRx * cosRy;
        return mat;
    }

    static Matrix4<T>
    rotate(T radians,
           T x,
           T y,
           T z)
        noexcept
    {
        const T x2 = x*x;
        const T y2 = y*y;
        const T cos = std::cos(radians);
        const T cosp = T(1.0) - cos;
        const T sin = std::sin(radians);

        return {
            cos + cosp * x2,
            cosp * x * y + z * sin,
            cosp * x * z - y * sin,
            T(0.0),

            cosp * x * y - z * sin,
            cos + cosp * y2,
            cosp * y * z + x * sin,
            T(0.0),

            cosp * x * z + y * sin,
            cosp * y * z - x * sin,
            cos + cosp * y2,
            T(0.0),

            T(0.0),
            T(0.0),
            T(0.0),
            T(1.0),
        };
    }

    static Matrix4<T>
    rotate(T radians,
           const Vector4<T>& axis)
    {
        return rotate(radians, axis[0], axis[1], axis[2]);
    }

    static Matrix4<T>
    scale(T sx,
          T sy,
          T sz)
        noexcept
    {
        Matrix4<T> mat = Matrix4<T>::identity();
        mat[0]  = sx;
        mat[5]  = sy;
        mat[10] = sz;
        return mat;
    }

    /**
     * Produce a 3D scaling matrix that scales uniformly in all directions.
     *
     * @param [in] s    Scaling factor.
     * @return A 4x4 scaling matrix.
     */
    static Matrix4<T>
    scale(T s)
        noexcept
    {
        return scale(s, s, s);
    }

    /** Self-typed move constructor. */
    Matrix4(const Matrix4<T>&& rhs)
        : Matrix<T,4,4>(rhs)
    { }

    /** Superclass-typed move constructor. */
    inline
    Matrix4(const Matrix<T,4,4>&& rhs)
        : Matrix<T,4,4>(rhs)
    { }
};


typedef Matrix4<int> IMatrix4;
typedef Matrix4<float> FMatrix4;
typedef Matrix4<double> DMatrix4;

} /* namespace math */
