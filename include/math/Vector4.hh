/* Vector4.hh
 * Eryn Wells <eryn@erynwells.me>
 */

#pragma once

#include "Matrix.hh"


namespace math {

template<typename T>
struct Vector4
    : public Vector<T, 4>
{
    inline
    Vector4()
        : Vector<T,4>()
    { }

    inline
    Vector4(T x,
            T y,
            T z,
            T w = 1.0)
        : Vector<T,4>({{x, y, z, w}})
    { }

    inline
    Vector4(const Vector<T,4>&& rhs)
        : Vector<T,4>(rhs)
    { }
};


typedef Vector4<int> IVector4;
typedef Vector4<float> FVector4;
typedef Vector4<double> DVector4;

} /* namespace math */
