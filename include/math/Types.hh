/* MathTypes.hh
 * Eryn Wells <eryn@erynwells.me>
 */

#pragma once


typedef unsigned int UInt;
typedef float Float;
