/* MatrixTest.cc
 * Eryn Wells <eryn@erynwells.me>
 */

#include <stdexcept>

#include "gtest/gtest.h"

#include "math/Matrix.hh"

using namespace math;


TEST(MatrixTest, GeneratesZeroMatrix) {
    auto mat = Matrix<int,4,4>::zero();
    for (size_t i = 0; i < mat.size(); i++) {
        EXPECT_EQ(mat[i], 0.0);
    }
}


TEST(MatrixTest, GeneratesIdentityMatrix) {
    auto mat = Matrix<int,4,4>::identity();
    for (int i = 0; i < Matrix<int,4,4>::sRows; i++) {
        for (int j = 0; j < Matrix<int,4,4>::sCols; j++) {
            EXPECT_EQ(mat(i,j), i == j ? 1.0 : 0.0);
        }
    }
}

#pragma mark - Matrix Operations

struct MatrixOperationTest
    : public ::testing::Test
{
protected:
    Matrix<int, 2, 2> mat1;
    Matrix<int, 2, 2> mat2;

    virtual void
    SetUp()
    {
        mat1[0] = 1;
        mat1[1] = 2;
        mat1[2] = 3;
        mat1[3] = 4;

        mat2[0] = 8;
        mat2[1] = 9;
        mat2[2] = 10;
        mat2[3] = 11;
    }
};


TEST_F(MatrixOperationTest, CopyConstruction)
{
    auto out(mat1);
    EXPECT_EQ(out, mat1);
}


TEST_F(MatrixOperationTest, Assignment)
{
    auto out = mat1;
    EXPECT_EQ(out, mat1);
}


TEST_F(MatrixOperationTest, ColumnMajorFormat)
{
    EXPECT_EQ(mat1(0,0), mat1[0]);
    EXPECT_EQ(mat1(0,1), mat1[1]);
    EXPECT_EQ(mat1(1,0), mat1[2]);
    EXPECT_EQ(mat1(1,1), mat1[3]);
}


TEST_F(MatrixOperationTest, EqualToSelf)
{
    EXPECT_EQ(mat1, mat1);
    EXPECT_NE(mat1, mat2);
}


/** Tests all the element access operators: operator(), and operator[]. */
TEST_F(MatrixOperationTest, ThrowsOutOfRangeExceptionForInvalidIndexes)
{
    // const operator()
    EXPECT_THROW(mat1(5,1), std::out_of_range);
    EXPECT_THROW(mat1(1,5), std::out_of_range);
    EXPECT_THROW(mat1(7,7), std::out_of_range);
    EXPECT_NO_THROW(mat1(0,0));
    EXPECT_NO_THROW(mat1(1,1));

    // reference operator()
    EXPECT_THROW(mat1(5,1) = 3.0, std::out_of_range);
    EXPECT_NO_THROW(mat1(1,1) = 4.0);

    // const operator[]
    EXPECT_THROW(mat1[18], std::out_of_range);
    EXPECT_NO_THROW(mat1[0]);
    EXPECT_NO_THROW(mat1[1]);

    // reference operator[]
    EXPECT_THROW(mat1[20] = 4.0, std::out_of_range);
    EXPECT_NO_THROW(mat1[3] = 12.0);
}


TEST_F(MatrixOperationTest, MatrixAddition)
{
    auto out = mat1 + mat1;
    EXPECT_EQ(out[0], 2);
    EXPECT_EQ(out[1], 4);
    EXPECT_EQ(out[2], 6);
    EXPECT_EQ(out[3], 8);

    auto zero = mat1 - mat1;
    for (size_t i = 0; i < zero.size(); i++) {
        EXPECT_EQ(zero[i], 0);
    }
}


TEST_F(MatrixOperationTest, ScalarMultiplication)
{
    Matrix<int, 2, 2> out = mat1 * 4;
    EXPECT_EQ(out[0], 4);
    EXPECT_EQ(out[1], 8);
    EXPECT_EQ(out[2], 12);
    EXPECT_EQ(out[3], 16);
}


TEST_F(MatrixOperationTest, ScalarMultiplicationInPlace)
{
    mat1 *= 6;
    EXPECT_EQ(mat1[0], 6);
    EXPECT_EQ(mat1[1], 12);
    EXPECT_EQ(mat1[2], 18);
    EXPECT_EQ(mat1[3], 24);
}


TEST_F(MatrixOperationTest, MatrixMultiplication)
{
    auto out = mat1 * mat2;
    EXPECT_EQ(out[0], 35);
    EXPECT_EQ(out[1], 52);
    EXPECT_EQ(out[2], 43);
    EXPECT_EQ(out[3], 64);
}


TEST_F(MatrixOperationTest, MatrixMultiplicationByIdentityGivesMatrix)
{
    auto out = Matrix<int,2,2>::identity() * mat1;
    EXPECT_EQ(out, mat1);
}


TEST_F(MatrixOperationTest, Negation)
{
    auto out = -mat1;
    EXPECT_EQ(out[0], -1);
    EXPECT_EQ(out[1], -2);
    EXPECT_EQ(out[2], -3);
    EXPECT_EQ(out[3], -4);
}
