/* Matrix4Test.cc
 * Eryn Wells <eryn@erynwells.me>
 */

#include "gtest/gtest.h"

#include "math/Matrix4.hh"
#include "math/Vector4.hh"

using namespace math;


struct Matrix4TransformTest
    : public ::testing::Test
{
protected:
    FVector4 v;

    virtual void
    SetUp()
    {
        v[0] = 1;
        v[1] = 2;
        v[2] = 3;
        v[3] = 1;
    }

    void
    checkVector(const FVector4& v,
                float x,
                float y,
                float z)
        const
    {
        EXPECT_FLOAT_EQ(v[0], x);
        EXPECT_FLOAT_EQ(v[1], y);
        EXPECT_FLOAT_EQ(v[2], z);
        EXPECT_FLOAT_EQ(v[3], 1.0);
    }

    void
    checkVector(const DVector4& v,
                double x,
                double y,
                double z)
        const
    {
        EXPECT_DOUBLE_EQ(v[0], x);
        EXPECT_DOUBLE_EQ(v[1], y);
        EXPECT_DOUBLE_EQ(v[2], z);
        EXPECT_DOUBLE_EQ(v[3], 1.0);
    }
};


TEST_F(Matrix4TransformTest, TranslationMatrixOnVector)
{
    EXPECT_EQ(Matrix4<float>::translate(1,2,3) * v, FVector4(2.0, 4.0, 6.0));
}


TEST_F(Matrix4TransformTest, IdentityTranslationMatrixOnVector)
{
    EXPECT_EQ(Matrix4<float>::translate(0,0,0) * v, v);
}


TEST_F(Matrix4TransformTest, RotationMatrixOnVector)
{
    checkVector(DMatrix4::rotate(0.0, 0.0, M_PI) * DVector4(1.0, 0.0, 0.0, 1.0), -1.0, 0.0, 0.0);
}


TEST_F(Matrix4TransformTest, IdentityRotationMatrixOnVector)
{
    EXPECT_EQ(FMatrix4::rotate(0.0, 0.0, 0.0) * v, v);
}


TEST_F(Matrix4TransformTest, AngleAxisRotationOnVector)
{
    checkVector(FMatrix4::rotate(M_PI, 0.0, 0.0, 1.0) * FVector4(1.0, 0.0, 0.0), -1.0, 0.0, 0.0);
}


TEST_F(Matrix4TransformTest, ScaleMatrixOnVector)
{
    checkVector(Matrix4<float>::scale(12.0, 12.0, 12.0) * v, 12.0, 24.0, 36.0);
    checkVector(Matrix4<float>::scale(16.0) * v, 16.0, 32.0, 48.0);
}


TEST_F(Matrix4TransformTest, IdentityScaleMatrixOnVector)
{
    EXPECT_EQ(Matrix4<float>::scale(1.0, 1.0, 1.0) * v, v);
    EXPECT_EQ(Matrix4<float>::scale(1.0) * v, v);
}
